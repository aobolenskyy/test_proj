from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    url(r'^$', 'my_app.views.home', name='home'),
    url(r'^process_data', 'my_app.views.process_data', name='process_data'),
    url(r'^admin/', include(admin.site.urls)),
)
