$(function(){
    $('body').on('click', 'td', function(e){
        var td_id = '#' + e.currentTarget.id;
        $('.new_value').hide();
        $('.old_value').show();
        $(td_id).find('.old_value').hide();
        $(td_id).find('.new_value').show()
    }).on('click', '.save', function(e){
        validate_and_save_new_value()
    }).on('keypress', function(e){
        var code = e.keyCode || e.which;
        if(code == 13 && !$(e.target.parentElement).hasClass('new_field')) validate_and_save(e, false);
    })
});

function get_table_info(table_name) {
    var url = '/process_data?model_name=' + table_name;
    $.getJSON(url,
        function(data){
            var html = generate_new_content(data),
                new_entry_html = generate_new_entry_html(data);
            $('.main_data').html(html);
            $('.new_entry').html(new_entry_html);
            $( ".datepicker" ).datepicker({
                onClose: function(selected_date){
                    if (!$(this.parentElement).hasClass('new_field')) validate_and_save(this, true)
                }
            });
        }
    ).fail(function() {
            $('.main_data').html('');
            $('.new_entry').html('');
        });
    return false;
}

function generate_new_content(data){
    var date_input = '<input type="text" class="datepicker">',
        text_input = '<input value="">',
        html = '<div class="row"><table class="table table-bordered table-striped fixed span12">';
    html += "<thead><tr>";
    $.each(data.field_names, function(i, val) {
        html += '<th>' + val.verbose_name + '</th>';
    });
    html += "</tr></thead><tbody>";
    $.each(data.all_data, function(i, val) {
        html += '<tr>';
        $.each(val, function(i, cur_val) {
            var hidden_input = text_input,
                element_id = cur_val.id + '__' + cur_val.model_name + '__' +
                    cur_val.field_name + '__' + cur_val.field_type;
            if (cur_val.field_type == 'DateField') hidden_input = date_input;
            html += '<td id="' + element_id + '"><span class="old_value">' + cur_val.value +
                '</span><span class="new_value" style="display: none" id="input__' +
                element_id + '">' + hidden_input +'</span></td>';
        });

        html += '</tr>';
    });
    html += "</tbody></table></div>";
    return html;
}

function generate_new_entry_html(data){
    var date_input = '<input type="text" class="datepicker">',
        text_input = '<input value="">',
        html = '';
    $.each(data.field_names, function(i, val) {
        var field_id = val.model_name + '__' + val.name + '__' + val.field_type;
        html += '<p>' + val.verbose_name + ': <span id="' + field_id + '" class="new_field ' + val.field_type + '">';
        if (val.field_type == 'DateField') html += date_input;
        else html += text_input;
        html += '</span></p>';
    });
    html += '<button class="btn save btn-success">Add</button>';
    return html;
}

function validate_and_save(e, date) {
    if (date) {
        var cur_input = e.parentNode,
            new_value = e.value,
            all_field_keys = cur_input.id.split('__')
    }
    else {
        var cur_input = e.target,
            new_value = cur_input.value,
            all_field_keys = cur_input.parentElement.id.split('__')}
    var data_type = all_field_keys[4];
    if (!new_value) {
        alert('Значение не введено');
        return false
    }
    if (data_type == 'IntegerField') {
        new_value = parseInt(new_value);
        if (isNaN(new_value)) { alert('Введите целое число'); return false}
    }
    if (data_type == 'DateField') {
        var new_value_list = new_value.split('/');
        if (new_value_list.length != 3) { alert('Введите коректную дату'); return false}
        new_value = new_value_list[2] + '-' + new_value_list[0] + '-' + new_value_list[1]
    }

    var new_value_dict = {
        'id': all_field_keys[1],
        'model_name': all_field_keys[2],
        'field': all_field_keys[3],
        'new_value': new_value
    };
    $('#id_new_value').val(JSON.stringify(new_value_dict));
    $('form').submit();
    return false
}
function validate_and_save_new_value(e, date) {
    var fields = new Array(),
        all_new_fielsd = $('.new_entry').find('span'),
        model_name = '',
        can_submit = true;

    $.each(all_new_fielsd, function(i, val) {
        var new_value = $(val).find('input').val(),
            all_field_keys = val.id.split('__'),
            field_name = all_field_keys[1],
            data_type = all_field_keys[2];
        model_name = all_field_keys[0];
        if (!new_value) {
            alert('Значение не введено');
            can_submit = false;
            return false
        }
        if (data_type == 'IntegerField') {
            new_value = parseInt(new_value);
            if (isNaN(new_value)) {
                alert('Введите целое число');
                can_submit = false;
                return false}
        }
        if (data_type == 'DateField') {
            var new_value_list = new_value.split('/');
            if (new_value_list.length != 3) {
                alert('Введите коректную дату');
                can_submit = false;
                return false}
            new_value = new_value_list[2] + '-' + new_value_list[0] + '-' + new_value_list[1]
        }
        fields.push({
            'field_name': field_name,
            'field_value': new_value
        })
    });

    if (can_submit){
        var new_value_dict = {
            'model_name': model_name,
            'fields': fields
        };
        $('#id_new_value').val(JSON.stringify(new_value_dict));
        $('form').submit();}
    return false
}