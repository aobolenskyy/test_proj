from annoying.decorators import render_to, ajax_request
from django.utils.encoding import force_unicode
from django.http.response import HttpResponseRedirect
from django.core.urlresolvers import reverse
from models import all_models
import json

@render_to('home.html')
def home(request):
    object_list = []
    for model_name, cur_model in all_models.items():
        object_list.append({
            'name': model_name,
            'verbose_name': cur_model._meta.verbose_name
        })
    context = {'object_list': object_list}
    return context


@ajax_request
def process_data(request):
    if request.method == 'POST':
        new_data = json.loads(request.POST.get('new_value'))
        model_name = new_data.get('model_name', False)
        model = all_models.get(model_name, False)
        if not model:
            return {'result': False}
        cur_id = new_data.get('id', False)
        if cur_id:
            field_to_update = new_data.get('field')
            new_value = new_data.get('new_value')
            model.objects.filter(id=cur_id).update(**{field_to_update: new_value})
        else:
            new_value = {}
            for field in new_data.get('fields'):
                new_value[field['field_name']] = field['field_value']
            model.objects.create(**new_value)
        return HttpResponseRedirect(reverse('home'))

    model_name = request.GET.get('model_name', False)
    model = all_models.get(model_name, False)
    if not model:
        return {}
    all_fields = [x for x in model._meta.get_all_field_names() if x != 'id']
    field_names = []
    for field in all_fields:
        cur_field = model._meta.get_field(field)
        field_names.append({
            'name': cur_field.name,
            'model_name': model_name,
            'verbose_name': cur_field.verbose_name,
            'field_type': cur_field.get_internal_type()

        })
    all_data = []
    field_to_unpack = ['id']
    field_to_unpack.extend(all_fields)
    for cur_object in model.objects.all().values_list(*field_to_unpack):
        cur_row_data = []
        row_id = cur_object[0]
        for i, row_value in enumerate(cur_object[1:]):
            cur_field = model._meta.get_field(all_fields[i])
            cur_row_data.append({
                'id': row_id,
                'model_name': model_name,
                'field_name': all_fields[i],
                'field_type': cur_field.get_internal_type(),
                'value': force_unicode(row_value),
            })
        all_data.append(cur_row_data)
    return {
        'all_data': all_data,
        'field_names': field_names}
