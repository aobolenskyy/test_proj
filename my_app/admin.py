from django.contrib import admin
from models import all_models


class Admin(admin.ModelAdmin):
    pass

for model in all_models.values():
    admin.site.register(model, Admin)
