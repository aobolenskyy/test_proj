from django.db import models
from yaml import load


FILE_WITH_MODELS = 'my_app/fixture.yml'
FIELD_TYPES = {
    'int': models.IntegerField,
    'char': models.CharField,
    'date': models.DateField
}

all_models = {}

file_to_load = open(FILE_WITH_MODELS)
data = load(file_to_load)
for model_name, model_data in data.items():
    class Meta:
        verbose_name = model_data['title']
        app_label = 'my_app'

    model_attrs = {
        '__module__': 'my_app',
        'Meta': Meta
    }
    for model_field in model_data['fields']:
        field_name = model_field['id']
        field_type = model_field['type']
        field_attrs = {
            'verbose_name': model_field['title']
        }
        if field_type == 'char':
            field_attrs['max_length'] = 255
        model_attrs[field_name] = FIELD_TYPES[field_type](**field_attrs)
    new_model = type(model_name, (models.Model, ), model_attrs)
    all_models.update({model_name: new_model})
