import json
from django.test import TestCase
from django.core.urlresolvers import reverse
from django.test.client import Client
from models import all_models


class TestWorkWithEntry(TestCase):
    def setUp(self):
        self.client = Client()
        model = all_models['rooms']
        model.objects.create(
            department='Old_department',
            spots=10
        )

    def tearDown(self):
        for model in all_models.values():
            model.objects.all().delete()

    def assertEntryExists(self, value):
        model = all_models['rooms']
        self.assertTrue(model.objects.filter(department=value).count())

    def test_create_entry(self):
        model_name = 'rooms'
        value = 'New_department'
        fields = [{'field_name': 'department', 'field_value': value},
                  {'field_name': 'spots', 'field_value': 20}]

        data = {'new_value': json.dumps({'model_name': model_name,
                                         'fields': fields})}
        self.client.post(reverse('process_data'), data=data)
        self.assertEntryExists(value)

    def test_update_entry(self):
        new_value = 'New_department'
        model = all_models['rooms']
        cur_id = model.objects.all()[0].id
        data = {'new_value':  json.dumps({
                'id': cur_id,
                'model_name': 'rooms',
                'field': 'department',
                'new_value': new_value})}

        self.client.post(reverse('process_data'), data=data)
        self.assertEntryExists(new_value)

    def test_home_page(self):
        response = self.client.get(reverse('home'))
        object_list = response.context['object_list']
        self.assertEqual(len(object_list), 2)
        all_names = [x['name'] for x in object_list]
        self.assertEqual(set(all_names), set(all_models.keys()))

    def test_get_entry(self):
        url = reverse('process_data')
        url += '?model_name=rooms'
        response = self.client.get(url)
        data = json.loads(response.content)['all_data'][0]
        dep_name = [x['value'] for x in data if x['field_name'] == 'department']
        self.assertEqual(len(data), 2)
        self.assertEqual(dep_name[0], 'Old_department')
